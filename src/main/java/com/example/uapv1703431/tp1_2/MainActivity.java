package com.example.uapv1703431.tp1_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.List;

;

public class MainActivity extends AppCompatActivity implements CustomAdapter.onClickListener {

    // create the listView
    RecyclerView lv;

    List<RowItem> rowItems;

    // get all Country
    String[] countryList = CountryList.getNameArray();
    int[] id = {R.drawable.flag_of_spain, R.drawable.flag_of_germany, R.drawable.flag_of_south_africa,
            R.drawable.flag_of_the_united_states, R.drawable.flag_of_france, R.drawable.flag_of_japan};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get the view
        lv = findViewById(R.id.recycle);
        RecyclerView.LayoutManager layout = new GridLayoutManager(this, 1);
        lv.setHasFixedSize(true);
        lv.setLayoutManager(layout);


        // create the adapter
        //ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, countryList);
        //lv.setAdapter(adapter);

        // create the custom adapter

        final CustomAdapter adapter = new CustomAdapter(id, countryList, this);
        lv.setAdapter(adapter);

        // listener on click to switch view
        /*
        lv.(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String country = parent.getAdapter().getItem(position).toString();

                Intent intent = new Intent(MainActivity.this, CountryActivity.class);

                intent.putExtra("country", country);

                startActivity(intent);
            }
        });
        */



    }


    @Override
    public void onClick(int position) {
        Log.d("debug", "click");
        String country = countryList[position];

        Intent intent = new Intent(MainActivity.this, CountryActivity.class);

        intent.putExtra("country", country);

        startActivity(intent);
    }
}
