package com.example.uapv1703431.tp1_2;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ImageViewHolder> {

    private int[] images;
    private String[] text;
    private onClickListener listener;

    public CustomAdapter(int[] images, String[] text, onClickListener listener) {
        this.images = images;
        this.text = text;
        this.listener = listener;
    }

    public String getText(int position) {
        return text[position];
    }

    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ImageViewHolder imh = new ImageViewHolder(view, listener);
        return imh;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        int image_id = images[position];
        holder.img.setImageResource(image_id);
        holder.text.setText(text[position]);
    }

    @Override
    public int getItemCount() {
        return this.images.length;
    }


    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView text;
        ImageView img;
        onClickListener listener;

        public ImageViewHolder(View itemView, onClickListener listener) {
            super(itemView);
            text = itemView.findViewById(R.id.title);
            img = itemView.findViewById(R.id.icon);
            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(getAdapterPosition());
        }
    }

    public interface onClickListener {
        void onClick(int position);
    }

}